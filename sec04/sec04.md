# 繰り返し処理と配列

Pythonの繰り返し処理は`while`と`for`があります。

## While

`while`文は条件式が`True`ならばwhile内の処理を繰り返し実行します。
`while`文は下のように書きます。`:`(コロン)とインデントを忘れずに!!

```python
while 条件式 :
    # 処理
```

## for

Pythonの`for`文は所謂`foreach`文で`インデックス`は`コレクション`の各要素を指します。
`コレクション`とは複数のデータを纏めたもので、配列や文字列、集合、tuple、rangeなどがあります。
Pythonのfor文は下のように書きます。`:`(コロン)とインデントを忘れずに!!

```python
for インデックス in コレクション:
    # 処理
```

### N回ループ

N回ループするには`range`使って下のように書きます。

```python
for i in range(10):
    print(i)
```

### インデックスがAから始まりBで終わるCずつ増えるループ

```python
for i in range(A,B,C):
    print(i)
```

A\<Bの時このコードはC++で書くと下のようになります。

```c
for(int i=A;i<B;i+=C){
    std::cout<<i<<std::endl;
}
```

### 配列の中身を全て表示

Pythonらしいfor文の使い方です。**慣れましょう**

```python
array = [1,2,3,5,8,13,21]

for i in array:
    print(i)
```

## 配列(list)

Pythonの配列(list型)は`変数名 = []`で宣言します。
配列の先頭のインデックス番号は`0`です。
また、配列に複数の型を混ぜることもできます。

