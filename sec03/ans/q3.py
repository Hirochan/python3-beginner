income = int(input())

rate=0.0
deduction=0.0

if income >= 40000:
    rate = 0.45
    deduction = 4796
elif income >= 18000:
    rate = 0.4
    deduction = 2796
elif income >= 9000:
    rate = 0.33
    deduction = 1536
elif income >= 6950:
    rate = 0.23
    deduction = 636
elif income >= 3300:
    rate = 0.2
    deduction = 427.5
elif income >= 1500:
    rate = 0.1
    deduction = 97.5
else:
    rate = 0.05
    deduction = 0

tax = income * rate - deduction
print(int(tax))