# 基礎

Pythonは**インデント**が非常に重要であるので、**必ずエディタ**(VimやVSCode、Emacsなど)を使って環境構築をしっかりとしましょう。

## 標準出力

`print()`関数で標準出力です

整数、小数、文字列、配列、辞書型など大体の型はいい感じに出力される

```python
print("Hello World")

print(1)

print(3.14)
```

```python
# 整数
a = 100

# 小数
b = 2.71828

# 配列
array = [1,2,3,5,8,13,21]

# 辞書
dictionary = {"Apple":150,"Banana":80,"Peach":300}

print(a)
# >>> 100

print(b)
# >>> 2.71828

print(array)
# >>> [1,2,3,5,8,13,21]

print(dictionary)
# >>> {'Apple': 150, 'Banana': 80, 'Peach': 300}
```

## 標準入力

`input()`関数で標準入力です。ただし、入力は全て**1つの文字列**(`string`)として受け取るので
使うときは、型を変換したりする必要があります。

`変数 = input()`の形で使います。

```python
# 文字列
msg = input()

# 整数
n = int(input())

# 小数
r = float(input())
```

## 基本演算

### 代入

`=`で変数に値を代入します。

```python
a = 12
print(a)

# >>> 12
```

### 加算

`+`の記号で加算を表します。

```python
a = 37
b = 12

print(a+b)

# >>> 49 
```

### 減算

`-`の記号で加算を表します。

```python
a = 37
b = 12

print(a-b)

# >>> 25
```

### 乗算

`*`の記号で乗算、`**`[^1]の記号で累乗を表します。

```python
a = 3
b = 2

print(a*b)

# >>> 6

print(a**b)
# >>> 9
```

### 除算

`/`の記号で除算、`//`の記号で切り捨て除算を表します。

`a//b`は$`[a/b]`$の値を返します。

```python
a = 3
b = 2

print(a/b)

# >>> 1.5

print(a//b)
# >>> 1
```

### 剰余

`%`剰余を表します。

```python
a = 7
b = 3

print(a%b)

# >>> 1
```

## 例題

### Q1

入力した文字をそのまま出力してください。

#### 入出力例

入力1
```
Hello World
```

出力1
```
Hello World
```

### Q2

二つの値を入力しその合計の3乗を出力してください。

#### 入力例

入力1
```
3
5
```

出力1
```
512
```

## 注釈

[^1]: 同じく累乗を表す**関数**で`pow()` もあるが、場合によって実行速度が異なるので注意([https://qiita.com/R_olldIce/items/ab3f382643e92122f8ef](https://qiita.com/R_olldIce/items/ab3f382643e92122f8ef))
