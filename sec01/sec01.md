# QuickStart

## 入手方法

### Linux / WSL

```sh
sudo apt update
sudo apt upgread -y
sudo apt install -y python3 python3-pip python3-venv
```

### Mac

標準で入っているはずなのでそれを使うか[homebrew](https://brew.sh/index_ja)を使ってインストールする

#### Homebrew

```sh
sudo brew install python3
```

### Windows

知らん

## 確認

`python3 --version`と打ってちゃんと表示されればOK