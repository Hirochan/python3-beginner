from cmath import sqrt
import math
Array = [60, 63, 68, 73, 74, 75, 76, 80, 83, 84, 89, 89, 90, 92, 93, 95, 98, 98, 99, 99]

# 平均値
ave = 0

# 中央値
mid = 0

# 標準偏差
std = 0

# ここから下編集可能
n = len(Array)
ave = sum(Array)/n

if n % 2 == 1:
    mid = Array[n//2]
else:
    mid = (Array[n//2-1] + Array[n//2])/2

S = 0
for i in Array:
    S += (i-ave)**2
std = math.sqrt(S/n)

# ここまで編集可能
print(f"平均 = {ave}")
print(f"中央値 = {mid}")
print(f"標準偏差 = {std}")