import statistics
Array = [60, 63, 68, 73, 74, 75, 76, 80, 83, 84, 89, 89, 90, 92, 93, 95, 98, 98, 99, 99]

# 平均値
ave = 0

# 中央値
med = 0

# 標準偏差
std = 0

# ここから下編集可能
ave = statistics.mean(Array)
med = statistics.median(Array)
std = statistics.pstdev(Array)
# ここまで編集可能
print(f"平均 = {ave}")
print(f"中央値 = {med}")
print(f"標準偏差 = {std}")